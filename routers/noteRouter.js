const express = require('express');
const router = express.Router();

const authMiddleware = require('../middlewares/authMiddleware');
const {
	getUserNotes,
	addUserNote,
	getNoteById,
	updateNoteById,
	replaceCheckNote,
	deleteNoteById,
} = require('../controllers/noteController');

router.post('/notes', authMiddleware, addUserNote);
router.get('/notes', authMiddleware, getUserNotes);
router.get('/notes/:id', authMiddleware, getNoteById);
router.put('/notes/:id', authMiddleware, updateNoteById);
router.patch('/notes/:id', authMiddleware, replaceCheckNote);
router.delete('/notes/:id', authMiddleware, deleteNoteById);

module.exports = router;
