const mongoose = require('mongoose');

const Schema = mongoose.Schema;

module.exports = mongoose.model(
	'note',
	new Schema(
		{
			userId: String,
			completed: Boolean,
			text: String,
			createdDate: Date,
		},
		{ versionKey: false }
	)
);
