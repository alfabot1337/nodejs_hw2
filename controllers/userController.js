const User = require('../models/userModel');

module.exports.userInfo = async (req, res) => {
	await User.findById(req.user._id)
		.exec()
		.then(({ _id, username, createdDate }) => {
			res.status(200).json({ _id, username, createdDate });
		})
		.catch(err => res.status(400).json({ message: err.message }));
};

module.exports.deleteUser = (req, res) => {
	User.deleteOne({ _id: req.user._id })
		.then(() => {
			res.status(200).json({ message: 'Success' });
		})
		.catch(err => res.status(400).json({ message: err.message }));
};

module.exports.changePassword = (req, res) => {
	const { oldPassword, newPassword } = req.body;
	User.findById(req.user._id)
		.exec()
		.then(user => {
			if (oldPassword !== user.password) {
				return res.status(400).json({ message: 'Old password is incorrect' });
			}
			if (newPassword === user.password) {
				return res
					.status(400)
					.json({ message: 'You are trying to use the same password' });
			}
			User.findByIdAndUpdate(user._id, { password: newPassword })
				.exec()
				.then(() => {
					res.status(200).json({ message: 'Success' });
				})
				.catch(err => res.status(400).json({ message: err.message }));
		})
		.catch(() => res.status(400).json({ message: 'User is incorrect' }));
};
