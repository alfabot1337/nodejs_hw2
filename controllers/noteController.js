const Note = require('../models/noteModel');

module.exports.getUserNotes = (req, res) => {
	Note.find({ userId: req.user._id })
		.exec()
		.then(notes => {
			res.status(200).json({ notes });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};
module.exports.addUserNote = (req, res) => {
	const createdDate = Date.now();
	const note = new Note({
		userId: req.user._id,
		completed: false,
		text: req.body.text,
		createdDate,
	});

	note
		.save()
		.then(() => {
			res.status(200).json({ message: 'Success' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};

module.exports.getNoteById = (req, res) => {
	Note.findById(req.params.id)
		.exec()
		.then(note => {
			if (!note) {
				return res.status(400).json({ message: 'Cant find node with this id' });
			}
			res.status(200).json({ note });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};

module.exports.updateNoteById = (req, res) => {
	Note.findByIdAndUpdate(req.params.id, { text: req.body.text })
		.exec()
		.then(() => {
			res.status(200).json({ message: 'Success' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};

module.exports.replaceCheckNote = (req, res) => {
	Note.findById(req.params.id)
		.exec()
		.then(note => {
			const completed = !note.completed;
			Note.findByIdAndUpdate(note.id, { completed })
				.exec()
				.then(() => {
					res.status(200).json({ message: 'Success' });
				})
				.catch(err => {
					res.status(400).json({ message: err.message });
				});
		})
		.catch(() => {
			res.status(400).json({ message: 'Note is incorrect' });
		});
};

module.exports.deleteNoteById = (req, res) => {
	Note.deleteOne({ _id: req.params.id })
		.exec()
		.then(x => {
			res.status(200).json({ message: 'Success' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};
