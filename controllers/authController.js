const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../config/auth');
const User = require('../models/userModel');

module.exports.register = (req, res) => {
	const { username, password } = req.body;
	const createdDate = Date.now();
	const user = new User({ username, password, createdDate });

	user
		.save()
		.then(() => {
			res.status(200).json({ message: 'Success' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};

module.exports.login = (req, res) => {
	const { username, password } = req.body;

	User.findOne({ username, password })
		.exec()
		.then(user => {
			if (!user) {
				return res
					.status(400)
					.json({ message: 'Username or password is incorrect!' });
			}
			res
				.status(200)
				.json({ jwt_token: jwt.sign(JSON.stringify(user), jwtSecret) });
		})
		.catch(err => res.status(500).json({ message: err.message }));
};
