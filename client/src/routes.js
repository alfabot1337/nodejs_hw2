import { Switch, Route, Redirect } from 'react-router-dom';
import { NotesPage } from './pages/NotesPage';
import { NotePage } from './pages/NotePage';
import { ProfilePage } from './pages/ProfilePage';
import { CreatePage } from './pages/CreatePage';
import { AuthPage } from './pages/AuthPage';

export const useRoutes = isAuthenticated => {
	if (isAuthenticated) {
		return (
			<Switch>
				<Route exact path="/notes">
					<NotesPage />
				</Route>
				<Route path="/notes/:id">
					<NotePage />
				</Route>
				<Route exact path="/create">
					<CreatePage />
				</Route>
				<Route exact path="/users/me">
					<ProfilePage />
				</Route>
				<Redirect to="/notes" />
			</Switch>
		);
	}
	return (
		<Switch>
			<Route exact path="/">
				<AuthPage />
			</Route>
			<Redirect to="/" />
		</Switch>
	);
};
