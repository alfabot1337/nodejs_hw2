export const ErrorMessage = ({ text }) => {
	return (
		<div className="alert alert-danger mt-4" role="alert">
			{text}
		</div>
	);
};
