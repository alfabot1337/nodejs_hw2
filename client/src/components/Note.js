import { useContext, useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const Note = ({ note }) => {
	const history = useHistory();
	const auth = useContext(AuthContext);
	const { request } = useHttp();
	const [completed, setCompleted] = useState(note.completed);

	const removeNote = async event => {
		event.preventDefault();
		try {
			await request(`api/notes/${note._id}`, 'DELETE', null, {
				Authorization: `Bearer ${auth.token}`,
			});
			history.push('/');
		} catch (error) {
			throw new Error(error);
		}
	};

	const markNote = async e => {
		e.preventDefault();
		try {
			await request(`api/notes/${note._id}`, 'PATCH', null, {
				Authorization: `Bearer ${auth.token}`,
			});
			setCompleted(!completed);
		} catch (error) {
			throw new Error(error);
		}
	};
	const getDate = () => {
		const date = new Date(Date.parse(note.createdDate));
		return date.toDateString();
	};

	return (
		<div
			className={`card mr-2 mb-2 ${completed ? 'border border-success' : ''}`}
			style={{ width: '18rem' }}
		>
			<div className="card-body">
				<p className="card-text">{note.text}</p>
				<i>{getDate()}</i> <br />
				<br />
				<NavLink to={`notes/${note._id}`} className="card-link">
					Change
				</NavLink>
				<NavLink to="/" className="card-link" onClick={markNote}>
					{completed ? 'Unmark' : 'Mark'}
				</NavLink>
				<NavLink to="/" className="card-link" onClick={removeNote}>
					Remove
				</NavLink>
			</div>
		</div>
	);
};
