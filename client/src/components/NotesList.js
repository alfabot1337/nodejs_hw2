import { Note } from './Note';

export const NotesList = ({ notes }) => {
	if (!notes.length) {
		return <h5>Here is clear. Please add note</h5>;
	}
	return notes.map(i => <Note note={i} key={i._id} />);
};
