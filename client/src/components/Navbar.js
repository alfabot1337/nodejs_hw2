import { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';

export const Navbar = () => {
	const auth = useContext(AuthContext);
	const logoutHandler = event => {
		event.preventDefault();
		auth.logout();
	};

	return (
		<nav className="navbar navbar-expand-lg navbar-light">
			<NavLink className="navbar-brand" to="/users/me">
				Profile
			</NavLink>
			<div className="collapse navbar-collapse" id="navbarNavAltMarkup">
				<ul className="navbar-nav">
					<li className="nav-item">
						<NavLink className="nav-link" to="/notes">
							Notes
						</NavLink>
					</li>
					<li className="nav-item">
						<NavLink className="nav-link" to="/create">
							Create
						</NavLink>
					</li>
				</ul>
				<NavLink
					className="nav-link ml-auto text-warning"
					to="/"
					onClick={logoutHandler}
				>
					Logout
				</NavLink>
			</div>
		</nav>
	);
};
