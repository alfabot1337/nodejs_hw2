import { useCallback, useContext, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const NotePage = () => {
	const id = useParams().id;
	const [note, setNote] = useState({ text: '' });
	const [text, setText] = useState('');
	const auth = useContext(AuthContext);
	const { request, loading } = useHttp();
	const history = useHistory();

	const fetchNote = useCallback(async () => {
		try {
			const { note: fetched } = await request(`/api/notes/${id}`, 'GET', null, {
				Authorization: `Bearer ${auth.token}`,
			});
			setNote(fetched);
		} catch (error) {
			throw new Error(error);
		}
	}, [request, auth.token, id]);

	useEffect(() => {
		fetchNote();
	}, [fetchNote]);

	const changeHandler = event => {
		setText(event.target.value);
	};
	const getDate = () => {
		const date = new Date(Date.parse(note.createdDate));
		return date.toDateString();
	};

	const updateNoteHandler = async () => {
		try {
			await request(
				`/api/notes/${id}`,
				'PUT',
				{ text },
				{
					Authorization: `Bearer ${auth.token}`,
				}
			);
			history.push('/notes');
		} catch (error) {
			throw new Error(error);
		}
	};

	return (
		<div>
			<h3>Note Page</h3>
			<div className="row">
				<div className="col-md-3">
					<p>{!loading && note.text}</p>
					<hr />
					<em className="mt-5">
						{!loading && getDate()}{' '}
						<span className="text-success">
							{note.completed && 'completed'}
						</span>
					</em>
				</div>
				<div className="col-md-6">
					<textarea
						name="text"
						id="text"
						cols="60"
						rows="5"
						placeholder="Text..."
						value={text}
						onChange={changeHandler}
					></textarea>
					<br />
					<button
						type="submit"
						className="btn btn-primary mr-auto"
						disabled={!text}
						onClick={updateNoteHandler}
					>
						Change
					</button>
				</div>
			</div>
		</div>
	);
};
