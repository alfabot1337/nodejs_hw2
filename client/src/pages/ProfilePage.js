import { useCallback, useContext, useEffect, useState } from 'react';
import { ErrorMessage } from '../components/ErrorMessage';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const ProfilePage = () => {
	const [user, setUser] = useState(null);
	const [password, setPassword] = useState({
		oldPassword: '',
		newPassword: '',
	});
	const { token, logout } = useContext(AuthContext);
	const { request, loading, error, clearError } = useHttp();

	const fetchUser = useCallback(async () => {
		try {
			const fetched = await request('/api/users/me', 'GET', null, {
				Authorization: `Bearer ${token}`,
			});
			setUser(fetched);
		} catch (e) {}
	}, [request, token]);

	useEffect(() => {
		fetchUser();
	}, [fetchUser]);

	const getDate = () => {
		const date = new Date(Date.parse(user.createdDate));
		return date.toDateString();
	};

	const changeHandler = event => {
		setPassword({ ...password, [event.target.name]: event.target.value });
	};

	const clearHandler = () => {
		setPassword({
			oldPassword: '',
			newPassword: '',
		});
	};

	const submitChangeHandler = async event => {
		event.preventDefault();
		try {
			await request(
				'/api/users/me',
				'PATCH',
				{ ...password },
				{
					Authorization: `Bearer ${token}`,
				}
			);
			clearHandler();
			clearError();
		} catch (e) {}
	};

	const deleteAccountHandler = async () => {
		try {
			setUser(null);
			logout();
			await request('/api/users/me', 'DELETE', null, {
				Authorization: `Bearer ${token}`,
			});
		} catch (e) {}
	};

	if (!loading && user) {
		return (
			<div className="row">
				<div className="col-md-6">
					<h3 className="mb-5">Profile Page</h3>
					<p>
						ID: <span> {user._id}</span>
					</p>
					<p>
						Your name: <span>{user.username}</span>
					</p>
					<p>
						Created date: <span>{getDate()}</span>
					</p>
					<button
						type="submit"
						className="btn btn-danger mr-auto mt-4"
						onClick={deleteAccountHandler}
					>
						Delete Accout
					</button>
				</div>
				<div className="col-md-6 mt-5">
					<form>
						<div className="form-group">
							<label htmlFor="oldPassword">Old Password</label>
							<input
								name="oldPassword"
								id="oldPassword"
								type="password"
								className="form-control"
								onChange={changeHandler}
								value={password.oldPassword}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="password">New Password</label>
							<input
								name="newPassword"
								id="newPassword"
								type="password"
								className="form-control"
								onChange={changeHandler}
								value={password.newPassword}
							/>
						</div>
						<button
							type="submit"
							className="btn btn-primary mr-3"
							disabled={loading}
							onClick={submitChangeHandler}
						>
							Submit
						</button>
						<button
							type="button"
							className="btn btn-success"
							disabled={loading}
							onClick={clearHandler}
						>
							Clear
						</button>
						{error && <ErrorMessage text={error} />}
					</form>
				</div>
			</div>
		);
	}

	return (
		<div>
			<h1>Profile Page!</h1>
			<p>Here is clear</p>
		</div>
	);
};
