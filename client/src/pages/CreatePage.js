import { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const CreatePage = () => {
	const auth = useContext(AuthContext);
	const { request } = useHttp();
	const history = useHistory();

	const [text, setText] = useState('');

	const changeHandler = event => {
		setText(event.target.value);
	};

	const createNote = async () => {
		try {
			await request(
				'api/notes',
				'POST',
				{ text },
				{
					Authorization: `Bearer ${auth.token}`,
				}
			);
			history.push('/notes');
		} catch (e) {}
	};

	return (
		<div>
			<h2>Create note</h2>
			<div className="row">
				<div className="col-md-6 offset-md-3">
					<textarea
						name="text"
						id="text"
						cols="60"
						rows="5"
						placeholder="Text..."
						value={text}
						onChange={changeHandler}
					></textarea>
					<br />
					<button
						type="submit"
						className="btn btn-primary mr-auto"
						onClick={createNote}
					>
						Create
					</button>
				</div>
			</div>
		</div>
	);
};
