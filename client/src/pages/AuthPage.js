import { useContext, useState } from 'react';
import { ErrorMessage } from '../components/ErrorMessage';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const AuthPage = () => {
	const auth = useContext(AuthContext);
	const { loading, request, error } = useHttp();
	const [form, setForm] = useState({
		username: '',
		password: '',
	});

	const changeHandler = event => {
		setForm({ ...form, [event.target.name]: event.target.value });
	};

	const registerHandler = async () => {
		try {
			await request('/api/auth/register', 'POST', { ...form });
			loginHandler();
		} catch (e) {}
	};

	const loginHandler = async () => {
		try {
			const data = await request('/api/auth/login', 'POST', { ...form });
			auth.login(data.jwt_token);
		} catch (e) {}
	};

	return (
		<div className="row">
			<div className="col-md-6 offset-md-3 pt-4">
				<h1 className="mb-4">Join or Sign UP</h1>
				<form>
					<div className="form-group">
						<label htmlFor="username">Username</label>
						<input
							name="username"
							id="username"
							type="username"
							className="form-control"
							placeholder="Username"
							value={form.username}
							onChange={changeHandler}
						/>
					</div>
					<div className="form-group">
						<label htmlFor="password">Password</label>
						<input
							name="password"
							id="password"
							type="password"
							className="form-control"
							placeholder="Password"
							value={form.password}
							onChange={changeHandler}
						/>
					</div>
					<button
						type="submit"
						className="btn btn-primary mr-3"
						disabled={loading}
						onClick={loginHandler}
					>
						Sign in
					</button>
					<button
						type="button"
						className="btn btn-success"
						disabled={loading}
						onClick={registerHandler}
					>
						Sign up
					</button>
				</form>

				{error && <ErrorMessage text={error} />}
			</div>
		</div>
	);
};
