import { useCallback, useContext, useEffect, useState } from 'react';
import { NotesList } from '../components/NotesList';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const NotesPage = () => {
	const [notes, setNotes] = useState([]);
	const auth = useContext(AuthContext);
	const { request, loading } = useHttp();

	const fetchNotes = useCallback(async () => {
		try {
			const { notes: fetched } = await request('api/notes', 'GET', null, {
				Authorization: `Bearer ${auth.token}`,
			});
			setNotes(fetched);
		} catch (error) {
			throw new Error(error);
		}
	}, [request, auth.token]);

	useEffect(() => {
		fetchNotes();
	}, [fetchNotes]);

	return (
		<div>
			<h1>Notes Page!</h1>
			<div className="row">{!loading && <NotesList notes={notes} />}</div>
		</div>
	);
};
