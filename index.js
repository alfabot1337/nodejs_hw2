const express = require('express');
const mongoose = require('mongoose');

const { port } = require('./config/server');
const { mongoUri } = require('./config/database');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/noteRouter');

const app = express();

async function start() {
	try {
		await mongoose.connect(mongoUri, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true,
		});
		app.listen(process.env.PORT || port, () => {
			console.log(`Server has been started at port ${port}`);
		});
	} catch (e) {
		console.log('Server error', e.message);
		process.exit(1);
	}
}

app.use(express.json());

app.use('/api', authRouter);
app.use('/api', userRouter);
app.use('/api', noteRouter);

start();
